﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.TeamPools
{
    public class EditModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public EditModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TeamPool TeamPool { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TeamPool = await _context.TeamPool
                .Include(t => t.LaPool)
                .Include(t => t.LaTeam).FirstOrDefaultAsync(m => m.ID == id);

            if (TeamPool == null)
            {
                return NotFound();
            }
           ViewData["PoolID"] = new SelectList(_context.Pool, "ID", "ID");
           ViewData["TeamID"] = new SelectList(_context.Team, "ID", "ID");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TeamPool).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TeamPoolExists(TeamPool.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TeamPoolExists(int id)
        {
            return _context.TeamPool.Any(e => e.ID == id);
        }
    }
}
