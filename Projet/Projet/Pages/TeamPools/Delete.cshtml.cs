﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.TeamPools
{
    public class DeleteModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DeleteModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TeamPool TeamPool { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TeamPool = await _context.TeamPool
                .Include(t => t.LaPool)
                .Include(t => t.LaTeam).FirstOrDefaultAsync(m => m.ID == id);

            if (TeamPool == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TeamPool = await _context.TeamPool.FindAsync(id);

            if (TeamPool != null)
            {
                _context.TeamPool.Remove(TeamPool);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("../Pools/Index");
        }
    }
}
