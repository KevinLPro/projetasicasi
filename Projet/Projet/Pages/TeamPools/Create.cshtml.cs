﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.TeamPools
{
    public class CreateModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;
        public Pool Pool { get; set; }
        [BindProperty]
        public TeamPool TeamPool { get; set; }
        public CreateModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Pool = await _context.Pool.FirstOrDefaultAsync(m => m.ID == (int)id);
            ViewData["TeamID"] = new SelectList(_context.Team.Where(item => item.validated == true), "ID", "Name") ;
            return Page();
        }

        //public IActionResult OnGet()
        //{
        //ViewData["PoolID"] = new SelectList(_context.Pool, "ID", "ID");
        //ViewData["TeamID"] = new SelectList(_context.Team, "ID", "ID");
        //return Page();
        //}

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            _context.TeamPool.Add(TeamPool);
            await _context.SaveChangesAsync();
            return RedirectToPage("../TeamPools/Index", new
            {
                id = TeamPool.PoolID
            });
        }
    }
}
