﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.TeamPools
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public IndexModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }
        public int PoolID { get; set; }
        public Pool Pool { get; set; }
        //public IList<TeamPool> TeamPool { get;set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //Récupération de l'ID de la pool dont on veut gérer les teams 
            PoolID = (int)id;
            //Pool = _context.Pool.Find(id);
            // Récupération des teams de cet pool 
            Pool = await _context.Pool
                .Include(i => i.TeamPools)
                .ThenInclude(i => i.LaTeam)
                .FirstOrDefaultAsync(m => m.ID == id);
            Pool = await _context.Pool
                .Include(p => p.PhasePool).FirstOrDefaultAsync(m => m.ID == id);
            //TeamPool = await _context.TeamPool
              //  .Include(e => e.LaTeam)
              //  .Where(e => e.LaTeam.ID == id)
              //  .Include(e => e.LaPool).ToListAsync();
            if (Pool == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
