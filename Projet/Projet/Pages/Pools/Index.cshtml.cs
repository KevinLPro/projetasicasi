﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Pools
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public IndexModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }
        public int PhaseID { get; set; }
        public IList<Pool> Pool { get;set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //Récupération de l'ID de la phase dont on veut gérer les pools 
            PhaseID = (int)id;
            Pool = await _context.Pool
                .Include(p => p.PhasePool )
                .Where(e => e.PhaseID == id)
                .ToListAsync();
            if (Pool == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
