﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Pools
{
    public class DetailsModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DetailsModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Pool Pool { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Pool = await _context.Pool
                .Include(i => i.TeamPools)
                .ThenInclude(i => i.LaTeam)
                .FirstOrDefaultAsync(m => m.ID == id);
            Pool = await _context.Pool
                .Include(p => p.PhasePool).FirstOrDefaultAsync(m => m.ID == id);

            if (Pool == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
