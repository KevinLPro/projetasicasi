﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Pools
{
    public class CreateModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public CreateModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Phase = await _context.Phase.FirstOrDefaultAsync(m => m.ID == (int)id);
            ViewData["PhaseID"] = new SelectList(_context.Phase.Where(item => item.ID == id), "ID", "ID");
            return Page();
        }

        [BindProperty]
        public Pool Pool { get; set; }
        [BindProperty]
        public Phase Phase { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Pool.Add(Pool);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Pools/Index", new
            {
                id = Phase.ID
            });
        }
    }
}
