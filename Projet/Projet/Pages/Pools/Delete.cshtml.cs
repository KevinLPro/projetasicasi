﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Pools
{
    public class DeleteModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DeleteModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Pool Pool { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Pool = await _context.Pool
                .Include(p => p.PhasePool).FirstOrDefaultAsync(m => m.ID == id);

            if (Pool == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Pool = await _context.Pool.FindAsync(id);

            if (Pool != null)
            {
                _context.Pool.Remove(Pool);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
