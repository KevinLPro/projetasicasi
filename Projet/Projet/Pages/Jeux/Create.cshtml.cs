﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Jeux
{
    public class CreateModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public CreateModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Jeu Jeu { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Jeux.Add(Jeu);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
