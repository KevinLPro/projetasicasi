﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Jeux
{
    public class DetailsModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DetailsModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Jeu Jeu { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jeu = await _context.Jeux.FirstOrDefaultAsync(m => m.ID == id);

            if (Jeu == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
