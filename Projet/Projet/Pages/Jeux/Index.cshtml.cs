﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Jeux
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public IndexModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Jeu> Jeu { get;set; }

        public async Task OnGetAsync()
        {
            Jeu = await _context.Jeux.ToListAsync();
        }
    }
}
