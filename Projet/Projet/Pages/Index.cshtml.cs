﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projet.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Projet.Pages
{
    [AllowAnonymous]
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        //CONTEXT
        private readonly Projet.Data.ApplicationDbContext _context;

        //LISTE TOURNOI
        public IList<Tournoi> lesTournoi { get; set; }

        public bool isEmpty;

        public bool isAuthentificated;

        public string userRole;
        public IndexModel(ILogger<IndexModel> logger, Projet.Data.ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public void OnGet()
        {
            lesTournoi = _context.Tournoi.ToList(); //Récupération de la liste des tournois
            isEmpty = lesTournoi.Any();
        }
    }
}
