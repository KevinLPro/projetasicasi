using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Projet.Model;

namespace Projet.Pages.Utilisateurs
{
    public class CreateModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _user;
        public CreateModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _context = context;
            _user = user;
        }
        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public string Username { get; set; }
        [BindProperty]
        public string Password { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {            
            var user = new ApplicationUser
            {
                UserName = Username,
                Email = Username,
                EmailConfirmed = true
            };
            var result = await _user.CreateAsync(user, Password);
            if (result.Succeeded)
            {
                var flag = false;
                var LicencieRole = _context.Roles.ToList();
                var role = "";
                //Gestion de la perte du role licencie
                foreach(var item in LicencieRole)
                {
                    if (item.Name == "Licencie")
                    {
                        flag = true;
                        role = item.Name;
                    }
                }
                if (!flag)
                {
                    _context.Roles.Add(new IdentityRole("Licencie")
                    {
                        NormalizedName = "LICENCIE"
                    });
                    result =  await _user.AddToRoleAsync(user, "Licencie");
                }
                else
                {
                    result = await _user.AddToRoleAsync(user, role);
                }
                
                if (result.Succeeded)
                {
                    return RedirectToPage("./Index");
                }
                else
                {
                    return RedirectToPage("./Create");
                }
            }
            else
            {
                return RedirectToPage("./Create");
            }
        }
    }
}
