using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Projet.Model;

namespace Projet.Pages.Utilisateurs
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _user;

        public IndexModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _context = context;
            _user = user;
        }
        public IList<ApplicationUser> Users { get; set; }
        public IList<ApplicationUser> UsersWithManagerRole { get; set; }
        public IList<ApplicationUser> UsersWithAdminRole { get; set; }

        public async Task OnGetAsync()
        {
            UsersWithManagerRole = await _user.GetUsersInRoleAsync("MANAGER");
            UsersWithAdminRole = await _user.GetUsersInRoleAsync("ADMINISTRATEUR");
            Users = await _user.GetUsersInRoleAsync("LICENCIE"); 
        }
    }
}
