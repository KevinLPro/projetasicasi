using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Projet.Model;

namespace Projet.Pages.Utilisateurs
{
    public class AddRoleModel : PageModel
    {
        [BindProperty]
        public ApplicationUser Utilisateur { get; set; }

        private readonly UserManager<ApplicationUser> _user;
        public string ts { get; set; }
        public AddRoleModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _user = user;
        }
        public async Task<IActionResult> OnGetAsync(string idUser)
        {
            Utilisateur = await _user.FindByIdAsync(idUser);

            ts = Utilisateur.UserName;
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(string idUser)
        {
            if (idUser == null)
            {
                return NotFound();
            }
            System.Diagnostics.Debug.Write(idUser);
            Utilisateur = await _user.FindByIdAsync(idUser);
            var result = await _user.RemoveFromRoleAsync(Utilisateur, "Licencie");
            var resultA = await _user.AddToRoleAsync(Utilisateur, "Manager");
            System.Diagnostics.Debug.Write(resultA);
            return RedirectToPage("./Index");
        }
    }
}
