﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Phases
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public IndexModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }
        public int TournoisID { get; set; }
        public IList<Phase> Phase { get;set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //Récupération de l'ID du tournois dont on veut gérer les phases 
            TournoisID = (int)id;
            Phase = await _context.Phase
                .Include(p => p.TournoiPhase)
                .Where(e => e.TournoiID == id)
                .ToListAsync();
            if (Phase == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
