﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;
using Projet.Model.FonctionnementPhase;

namespace Projet.Pages.Phases
{
    public class DetailsModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DetailsModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Fonctionnement fonctionnement;
        public Phase Phase { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            Phase = await _context.Phase
                .Include(p => p.TournoiPhase).FirstOrDefaultAsync(m => m.ID == id);
            fonctionnement = Phase.classement();
            if (Phase == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
