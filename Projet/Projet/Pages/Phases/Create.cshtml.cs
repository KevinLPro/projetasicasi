﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Phases
{
    public class CreateModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public CreateModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Tournoi = await _context.Tournoi.FirstOrDefaultAsync(m => m.ID == (int)id);
            ViewData["TournoiID"] = new SelectList(_context.Tournoi.Where(item => item.ID == id), "ID", "Name");
            return Page();
        }
        [BindProperty]
        public Phase Phase { get; set; }
        [BindProperty]
        public Tournoi Tournoi { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Phase.Add(Phase);
            await _context.SaveChangesAsync();
            return RedirectToPage("../Phases/Index", new
            {
                id = Tournoi.ID
            });
        }
    }
}
