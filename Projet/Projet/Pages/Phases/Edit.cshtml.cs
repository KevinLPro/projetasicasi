﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Phases
{
    public class EditModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public EditModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Phase Phase { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Phase = await _context.Phase
                .Include(p => p.TournoiPhase).FirstOrDefaultAsync(m => m.ID == id);

            if (Phase == null)
            {
                return NotFound();
            }
           ViewData["TournoiID"] = new SelectList(_context.Tournoi, "ID", "ID");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Phase).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PhaseExists(Phase.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool PhaseExists(int id)
        {
            return _context.Phase.Any(e => e.ID == id);
        }
    }
}
