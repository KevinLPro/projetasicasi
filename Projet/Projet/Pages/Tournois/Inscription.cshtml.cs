using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class InscriptionModel : PageModel
    {
        public int? ConnectedUserTeamID;
        public string username;
        private readonly Projet.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _user;
        public InscriptionModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _context = context;
            _user = user;
        }

        [BindProperty]
        public Tournoi Tournoi { get; set; }
        public Team Team { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            username = User.Identity.Name;
            var myUser = await _user.FindByNameAsync(username);
            string idUser = myUser.Id;
            var utilisateur = _context.Users.Find(idUser);
            ApplicationUser uti = (ApplicationUser)utilisateur;
            ConnectedUserTeamID = uti.TeamID;

            if (id == null || ConnectedUserTeamID == null)
            {
                return NotFound();
            }

            Tournoi = await _context.Tournoi.FirstOrDefaultAsync(m => m.ID == id);
            Team = await _context.Team.FirstOrDefaultAsync(m => m.ID == ConnectedUserTeamID);

            if (Tournoi == null || Team == null)
            {
                return NotFound();
            }
            return Page();
        }
        //POSTSUBMIT
        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            username = User.Identity.Name;
            var myUser = await _user.FindByNameAsync(username);
            string idUser = myUser.Id;
            var utilisateur = _context.Users.Find(idUser);
            ApplicationUser uti = (ApplicationUser)utilisateur;
            ConnectedUserTeamID = uti.TeamID;

            Tournoi = _context.Tournoi.FirstOrDefaultAsync(m => m.ID == id).Result;
            Team = await _context.Team.FirstOrDefaultAsync(m => m.ID == ConnectedUserTeamID);
            var Team_TournoiAdd = new Team_Tournoi { LTeam = Team, LTeamID = Team.ID, LTournoi = Tournoi, LTournoiID = Tournoi.ID };
            _context.Add(Team_TournoiAdd);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
