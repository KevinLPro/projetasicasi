using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class DesinscriptionModel : PageModel
    {
        public string username;
        private readonly Projet.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _user;
        public DesinscriptionModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _context = context;
            _user = user;
        }

        [BindProperty]
        public Tournoi Tournoi { get; set; }

        public Team Team { get; set; }
        public int? ConnectedUserTeamID { get; private set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {

            username = User.Identity.Name;
            var myUser = await _user.FindByNameAsync(username);
            string idUser = myUser.Id;
            var utilisateur = _context.Users.Find(idUser);
            ApplicationUser uti = (ApplicationUser)utilisateur;
            ConnectedUserTeamID = uti.TeamID;

            if (id == null)
            {
                return NotFound();
            }

            Team = await _context.Team.FirstOrDefaultAsync(m => m.ID == ConnectedUserTeamID);
            Tournoi = await _context.Tournoi.FirstOrDefaultAsync(m => m.ID == id);

            if (Tournoi == null || Team == null)
            {
                return NotFound();
            }
            return Page();
        }
        //POSTSUBMIT
        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            username = User.Identity.Name;
            var myUser = await _user.FindByNameAsync(username);
            string idUser = myUser.Id;
            var utilisateur = _context.Users.Find(idUser);
            ApplicationUser uti = (ApplicationUser)utilisateur;
            ConnectedUserTeamID = uti.TeamID;

            Team_Tournoi Inscription = _context.Team_Tournoi.Where(d => d.LTeamID == ConnectedUserTeamID).FirstOrDefaultAsync().Result;

            if (Inscription != null)
            {
                _context.Team_Tournoi.Remove(Inscription);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
