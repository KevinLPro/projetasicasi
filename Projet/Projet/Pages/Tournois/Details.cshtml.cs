﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class DetailsModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DetailsModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Tournoi Tournoi { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tournoi = await _context.Tournoi.FirstOrDefaultAsync(m => m.ID == id);

            if (Tournoi == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
