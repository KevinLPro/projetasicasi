﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class DeleteModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public DeleteModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tournoi Tournoi { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tournoi = await _context.Tournoi.FirstOrDefaultAsync(m => m.ID == id);

            if (Tournoi == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tournoi = await _context.Tournoi.FindAsync(id);

            if (Tournoi != null)
            {
                _context.Tournoi.Remove(Tournoi);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
