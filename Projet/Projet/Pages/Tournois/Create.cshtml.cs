﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Projet.Autorization;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class CreateModel : DI_BasePageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public CreateModel(Projet.Data.ApplicationDbContext context, IAuthorizationService authorizationService, UserManager<ApplicationUser> userManager) : base(context, authorizationService, userManager)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["JeuTournoiID"] = new SelectList(_context.Jeux, "ID", "Nom");
            return Page();
        }

        [BindProperty]
        public Tournoi Tournoi { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Tournoi.OwnerID = UserManager.GetUserId(User);

            var isAuthorized = await AuthorizationService.AuthorizeAsync(User, Tournoi, TournoiOperation.Create);

            if (!isAuthorized.Succeeded)
            {
                return Forbid();
            }

            _context.Tournoi.Add(Tournoi);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
