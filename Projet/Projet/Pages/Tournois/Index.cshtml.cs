﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Autorization;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Tournois
{
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _user;

        public int? ConnectedUserTeamID; 
        public ApplicationUser utilisateur { get; set; }

        public string username;
        public IndexModel(Projet.Data.ApplicationDbContext context, UserManager<ApplicationUser> user)
        {
            _context = context;
            _user = user;
        }

        public IList<Tournoi> Tournoi { get;set; }
        public IList<Team_Tournoi> TeamTournoi { get;set; }

        public async Task OnGetAsync()
        {
            //var Tournoi = from c in _context.Tournoi select c;

            var isAuthorized = User.IsInRole(Constants.TournoiAdministrateurRole) || User.IsInRole(Constants.TournoiManagerRole);

            
            username = User.Identity.Name;
            var myUser = await _user.FindByNameAsync(username);
            string idUser = myUser.Id;
            var utilisateur = _context.Users.Find(idUser);
            ApplicationUser uti = (ApplicationUser)utilisateur;
            ConnectedUserTeamID = uti.TeamID;
            //recuperation des jeux
            Tournoi = await _context.Tournoi
                .Include(t => t.JeuTournoi).ToListAsync();
            //Toutes les participation de la team de l'user
            TeamTournoi = _context.Team_Tournoi
                .Where(d => d.LTeamID == ConnectedUserTeamID)
                .OrderBy(d => d.LTournoiID).ToList();
            Tournoi = await _context.Tournoi.ToListAsync();
        }
    }
}
