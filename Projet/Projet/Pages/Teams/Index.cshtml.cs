﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Projet.Data;
using Projet.Model;

namespace Projet.Pages.Teams
{   
    public class IndexModel : PageModel
    {
        private readonly Projet.Data.ApplicationDbContext _context;

        public IndexModel(Projet.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Team> Equipe { get;set; }
        public IList<Team> NonValidatedTeam { get;set; }

        public async Task OnGetAsync()
        {
            //Check si l'utilisateur est dans une team

            //On récupère la team de l'utilisateur

            //On récupère les autres
            Equipe = await _context.Team.Where(d => d.validated == true).ToListAsync();
            NonValidatedTeam = await _context.Team.Where(d => d.validated == false).ToListAsync();
        }
    }
}
