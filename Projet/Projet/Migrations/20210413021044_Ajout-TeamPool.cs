﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Projet.Migrations
{
    public partial class AjoutTeamPool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Pool_PoolID",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_PoolID",
                table: "Team");

            migrationBuilder.CreateTable(
                name: "TeamPool",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PoolID = table.Column<int>(type: "int", nullable: false),
                    TeamID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamPool", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TeamPool_Pool_PoolID",
                        column: x => x.PoolID,
                        principalTable: "Pool",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeamPool_Team_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Team",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TeamPool_PoolID",
                table: "TeamPool",
                column: "PoolID");

            migrationBuilder.CreateIndex(
                name: "IX_TeamPool_TeamID",
                table: "TeamPool",
                column: "TeamID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TeamPool");

            migrationBuilder.CreateIndex(
                name: "IX_Team_PoolID",
                table: "Team",
                column: "PoolID");

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Pool_PoolID",
                table: "Team",
                column: "PoolID",
                principalTable: "Pool",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
