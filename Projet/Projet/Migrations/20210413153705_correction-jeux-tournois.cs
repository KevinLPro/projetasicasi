﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Projet.Migrations
{
    public partial class correctionjeuxtournois : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Jeux_Tournoi");

            migrationBuilder.AddColumn<int>(
                name: "JeuTournoiID",
                table: "Tournoi",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tournoi_JeuTournoiID",
                table: "Tournoi",
                column: "JeuTournoiID");

            migrationBuilder.AddForeignKey(
                name: "FK_Tournoi_Jeux_JeuTournoiID",
                table: "Tournoi",
                column: "JeuTournoiID",
                principalTable: "Jeux",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tournoi_Jeux_JeuTournoiID",
                table: "Tournoi");

            migrationBuilder.DropIndex(
                name: "IX_Tournoi_JeuTournoiID",
                table: "Tournoi");

            migrationBuilder.DropColumn(
                name: "JeuTournoiID",
                table: "Tournoi");

            migrationBuilder.CreateTable(
                name: "Jeux_Tournoi",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LJeuxID = table.Column<int>(type: "int", nullable: false),
                    LTournoiID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jeux_Tournoi", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Jeux_Tournoi_Jeux_LJeuxID",
                        column: x => x.LJeuxID,
                        principalTable: "Jeux",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Jeux_Tournoi_Tournoi_LTournoiID",
                        column: x => x.LTournoiID,
                        principalTable: "Tournoi",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jeux_Tournoi_LJeuxID",
                table: "Jeux_Tournoi",
                column: "LJeuxID");

            migrationBuilder.CreateIndex(
                name: "IX_Jeux_Tournoi_LTournoiID",
                table: "Jeux_Tournoi",
                column: "LTournoiID");
        }
    }
}
