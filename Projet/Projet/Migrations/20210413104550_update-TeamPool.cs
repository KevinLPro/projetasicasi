﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Projet.Migrations
{
    public partial class updateTeamPool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pool_Phase_PhaseID",
                table: "Pool");

            migrationBuilder.DropColumn(
                name: "PoolID",
                table: "Team");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Team",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhaseID",
                table: "Pool",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Pool",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Pool_Phase_PhaseID",
                table: "Pool",
                column: "PhaseID",
                principalTable: "Phase",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pool_Phase_PhaseID",
                table: "Pool");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Team",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<int>(
                name: "PoolID",
                table: "Team",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhaseID",
                table: "Pool",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Pool",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddForeignKey(
                name: "FK_Pool_Phase_PhaseID",
                table: "Pool",
                column: "PhaseID",
                principalTable: "Phase",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
