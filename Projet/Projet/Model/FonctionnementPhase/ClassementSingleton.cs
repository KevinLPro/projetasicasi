﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model.FonctionnementPhase
{
    public sealed class Classement
    {
        private static Classement instance = null;
        private static readonly object padlock = new object();
        private List<int> classement;
        Classement()
        {
        }

        public static Classement Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new Classement();
                    }
                    return instance;
                }
            }
        }
        public void loadClassement(List<int> classement)
        {
            this.classement = classement;
        }
        public void clearClassement()
        {
            this.classement = null;
        }
        public void newClassement(List<Team> team)
        {
            clearClassement();
            classement = new List<int>();
            for(int i = 0; i < team.Count; i++)
            {
                classement.Add(0);
            }
        }
        public void addPoint(int value, int index)
        {
            classement.Insert(index, value);
        }

    }
}
