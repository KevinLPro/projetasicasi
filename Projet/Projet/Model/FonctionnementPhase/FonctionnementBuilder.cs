﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model.FonctionnementPhase
{
    public abstract class FonctionnementBuilder
    {
        protected Fonctionnement fonctionnement;

        public Fonctionnement getFonctionnement()
        {
            return fonctionnement;
        }
        public void createNewFonctionnementProduct()
        {
            fonctionnement = new Fonctionnement();
        }

        public abstract void buildListeTeam(List<Team> listeTeam);
    }
}
