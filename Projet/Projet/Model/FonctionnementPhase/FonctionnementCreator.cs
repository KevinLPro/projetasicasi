﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model.FonctionnementPhase
{
    public class FonctionnementCreator
    {
        private FonctionnementBuilder fonctionBuilder;

        public void setFonctionnementBuilder(FonctionnementBuilder fb)
        {
            fonctionBuilder = fb;
        }
        public Fonctionnement getFonctionnement()
        {
            return fonctionBuilder.getFonctionnement();
        }

        public void createFonctionnement()
        {
            fonctionBuilder.createNewFonctionnementProduct(); 
        }
    }
}
