﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model.FonctionnementPhase
{
    public class EliminationDirectSimple : FonctionnementBuilder
    {
        public EliminationDirectSimple(List<Team> listeTeam)
        {
            buildListeTeam(listeTeam);
            Classement c = Classement.Instance;
            c.newClassement(listeTeam);
            MancheNonTerminale root = new MancheNonTerminale(listeTeam, null); //Création d'un arbre par propagation dans le constructeur
        }

        public override void buildListeTeam(List<Team> listeTeam)
        {
            fonctionnement.setTeamList(listeTeam);
        }
    }
}
