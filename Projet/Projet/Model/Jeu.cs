﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Jeu
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public string OwnerID { get; set; } //Créateur du jeu

        //Lien OneToMany
        [Display(Name = "Les Tournois du jeu")]
        public ICollection<Tournoi> listeTournoi { get; set; }
    }
}
