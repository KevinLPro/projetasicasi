﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Pool
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]

        //Reference to Phase
        public int? PhaseID { get; set; }
        public Phase PhasePool { get; set; }
        // Lien de navigation ManyToMany
        [Display(Name = "Teams de la Pool")]
        public ICollection<TeamPool> TeamPools { get; set; }
    }
}
