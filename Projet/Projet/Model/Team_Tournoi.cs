﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Team_Tournoi
    {
        public int ID { get; set; }
        public int pointTeamTournoi { get; set; }

        //Lien de composition vers Team
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LTeamID { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Team LTeam { get; set; }

        //Lien de composition vers Tournoi
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LTournoiID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Tournoi LTournoi { get; set; }
        
    }
}
