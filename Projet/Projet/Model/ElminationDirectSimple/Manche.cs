﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public abstract class Manche
    {
        protected string name;
        public Manche()
        {
            name = "";
        }
        public Manche(string name)
        {
            this.name = name;
        }
        public void setNomManche(string name)
        {
            this.name = name;
        }
        public abstract bool add(Manche m);
        public abstract bool remove(Manche m);

        public bool display(int indent)
        {
            for(int i = 0; i < indent; i++)
            {
                Console.WriteLine("\t");
            }
            Console.WriteLine(name);
            return true;
        }
    }
}
