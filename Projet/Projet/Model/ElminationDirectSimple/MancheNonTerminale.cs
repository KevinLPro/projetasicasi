﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class MancheNonTerminale : Manche{
        private List<Manche> elements;
        private List<Team> listTeam;
        private List<int> Classement;
        public MancheNonTerminale(List<Team> teams, MancheNonTerminale parent)
        {
            if(parent == null)
            {

            }
            if(teams.Count > 2)
            {
                List<Team> firstHalf = teams.GetRange(0, (int)teams.Count / 2);
                List<Team> secondHalf = teams.GetRange((int)teams.Count / 2, teams.Count);
                this.add(new MancheNonTerminale(firstHalf, this));
                this.add(new MancheNonTerminale(secondHalf, this));
                this.add(new Rencontre(parent));
            }
            else if(teams.Count == 2)
            {
                this.add(new Rencontre(teams.ElementAt(0), teams.ElementAt(1), parent));
            }
        }


        public override bool add(Manche m)
        {
            elements.Add(m);
            return true;
        }

        public override bool remove(Manche m)
        {
            elements.Remove(m);
            return true;
        }
        public Rencontre FindRencontreInList()
        {
            foreach(var manche in elements)
            {
                if(manche is Rencontre)
                {
                    return (Rencontre)manche; 
                }
            }
            return null;
        }
        public bool display(int indent) 
        {
            base.display(indent);
            for(int i = 0; i < indent; i++)
            {
                ((Manche)elements.ElementAt(i)).display(indent + 1);
            }
            return true;
        }
    }
}
