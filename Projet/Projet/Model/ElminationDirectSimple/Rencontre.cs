﻿using Projet.Model.FonctionnementPhase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Rencontre : Manche
    {
        private List<Team> rencontre;
        MancheNonTerminale MancheParent;
        public Rencontre(MancheNonTerminale parent)
        {
            MancheParent = parent;
        }
        public Rencontre(Team teamA, Team teamB, MancheNonTerminale MancheParent)
        {
            rencontre.Add(teamA);
            rencontre.Add(teamB);
            this.MancheParent = MancheParent;  
        }
        /**
         * On déclare le gagnant de cette rencontre et ont le fait monter a la manche suivante
         */

        public bool setGagnant(int id)
        {
            Team victoire = rencontre.Find(u => u.ID == id);
            Classement c = Classement.Instance;
            var indexElement = rencontre.FindIndex(a => a.ID == id);
            c.addPoint(1, indexElement);
            if (MancheParent != null)
            {
                Rencontre RencontreSuivante = MancheParent.FindRencontreInList();
                RencontreSuivante.addElementToList(victoire);
                return true;
            }
            return false;
        }
        public Team getGagnant(int id)
        {
            Team victoire = rencontre.Find(u => u.ID == id);
            return victoire;
        }
        public void addElementToList(Team team)
        {
            rencontre.Add(team);
        }
        public override bool add(Manche m)
        {
            Console.WriteLine("Impossible d'ajouter un element");
            return false;
        }

        public override bool remove(Manche m)
        {
            Console.WriteLine("Impossible d'enlever un element");
            return false;
        }
    }
}
