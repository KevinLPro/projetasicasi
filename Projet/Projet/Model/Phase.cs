﻿using Projet.Model.FonctionnementPhase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Projet.Model
{
    public class Phase
    {
        public int ID { get; set; }
        public int? TournoiID { get; set; } //Tournoi auquel la pool correspond
        public Tournoi TournoiPhase { get; set; }
        public int fonctionnement { get; set; }

        [Display(Name = "Pools de la Phase")]
        public ICollection<Pool> Pool { get; set; }

        public enum MesFonctionnements : int
        { 
            Elimination = 1,
            RoundRobin = 2
        }

        public Fonctionnement classement()
        {
            FonctionnementCreator creator = new FonctionnementCreator();
            FonctionnementBuilder fonctionnementObject = null;
            if (fonctionnement == 1)
            {
                fonctionnementObject = new EliminationDirectSimple(GetTeams());
            }
            if (fonctionnement == 2)
            {
                fonctionnementObject = new RoundRobin();
            }
            creator.setFonctionnementBuilder(fonctionnementObject);

            creator.createFonctionnement();

            return creator.getFonctionnement();

        }
        public List<Team> GetTeams()
        {
            List<Team> list = new List<Team>();
            foreach (Pool p in Pool)
            {
                foreach (TeamPool t in p.TeamPools)
                {
                    list.Add(t.LaTeam);
                }
            }
            return list;
        }
    }
}
