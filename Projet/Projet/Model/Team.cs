﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Team
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }

        public string OwnerID { get; set; } //Créateur de la team

        public bool validated { get; set; }

        [Display(Name = "Les tournoi ou la team participe")]
        public ICollection<Team_Tournoi> listeParticipationTournoi { get; set; }

        // Lien de navigation ManyToMany
        [Display(Name = "Pools de la Team")]
        public ICollection<TeamPool> TeamPools { get; set; }
        public ICollection<ApplicationUser> InscritTeam { get; set; }
    }
}
