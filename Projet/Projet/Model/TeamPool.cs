﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class TeamPool
    {
        //Clé primaire 
        public int ID { get; set; }
        //Lien de composition vers l'enseignant 
        public int PoolID { get; set; }
        public Pool LaPool { get; set; }
        //Lien de composition vers l'UE 
        public int TeamID { get; set; }
        public Team LaTeam { get; set; }
    }
}
