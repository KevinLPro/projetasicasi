﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Model
{
    public class Tournoi
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public string OwnerID { get; set; } //Créateur du tournoi
        public ICollection<Phase> PhaseTournoi { get; set; }

        //Reference to Jeu 
        public int? JeuTournoiID { get; set; }
        public Jeu JeuTournoi { get; set; }

        [Display(Name = "Les équipes participant au tournoi")]
        public ICollection<Team_Tournoi> listeParticipationTournoi { get; set; }

    }
}
