﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Projet.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Projet.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Jeu> Jeux { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Team_Tournoi> Team_Tournoi { get; set; }
        public DbSet<Tournoi> Tournoi { get; set; }
        public DbSet<Phase> Phase { get; set; }
        public DbSet<Pool> Pool { get; set; }
        public DbSet<ApplicationUser> ApplicationUser { get; set; }
        public DbSet<Projet.Model.TeamPool> TeamPool { get; set; }

    }
}
