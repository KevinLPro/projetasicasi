﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Projet.Data.Migrations
{
    public partial class ajoutValidatedTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "validated",
                table: "Team",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "validated",
                table: "Team");
        }
    }
}
