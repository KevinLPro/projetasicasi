﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projet.Data
{
    public static class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider, string testUserPw)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                SeedDB(context, "0");
            }
        }

        public static void SeedDB(ApplicationDbContext context, string adminID)
        {
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            context.Users.AddRange(
                new Model.ApplicationUser
                {
                    UserName = "test@gmail.com",
                    Email = "test@gmail.com",
                    EmailConfirmed = true,
                },
                new Model.ApplicationUser
                {
                    UserName = "azer@gmail.com",
                    Email = "azer@gmail.com",
                    EmailConfirmed = true,
                }
            );
            context.SaveChanges();
        }
    }
}
