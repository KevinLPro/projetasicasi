﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Projet.Autorization
{
    public class TournoiOperation
    {
        public static OperationAuthorizationRequirement Create = new OperationAuthorizationRequirement { Name = Constants.CreateOperationName };
        public static OperationAuthorizationRequirement Read = new OperationAuthorizationRequirement { Name = Constants.CreateOperationName };
        public static OperationAuthorizationRequirement Update = new OperationAuthorizationRequirement { Name = Constants.CreateOperationName };
        public static OperationAuthorizationRequirement Delete = new OperationAuthorizationRequirement { Name = Constants.CreateOperationName };
    }

    public class Constants
    {
        public static readonly string CreateOperationName = "Create";
        public static readonly string ReadOperationName = "Read";
        public static readonly string UpdateOperationName = "Update";
        public static readonly string DeleteOperationName = "Delete";

        public static readonly string TournoiAdministrateurRole = "TournoiAdministrateur";
        public static readonly string TournoiManagerRole = "TournoiManager";

        public static readonly string InscriptionTournoiTeam = "Inscrire son équipe";
        public static readonly string DesinscriptionTournoiTeam = "Desinscrire son équipe";
    }
}
